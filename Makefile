
ROOT_DIR = .

OUTPUT_DIR = ./public


TEMPLATE = --template=build/template.html
INCLUDE_CSSS = --css=/assets/latex_style.css --css=/assets/style.css
TOC = --table-of-contents
PANDOC_FLAGS = $(TEMPLATE) $(INCLUDE_CSSS) --mathjax -V lang="ja"

all: copy_asset root_md_langs bibliography cp_data_files \
	nilbilus_posts nilbilus_datas nilbilus_index \
	#email_png

#
#INDEX_SOURCE = writtings/index.tex
#index: $(INDEX_SOURCE)
#	pandoc $(INDEX_SOURCE) --to=html5 -o public/index.html \
#	$(PANDOC_FLAGS)


# copy All png files under writtings/data/ to public/data
#CP_DATAFILES := $(shell find writtings/data/ -type f \( -name '*.png' \))
#cp_data_files : $(CP_DATAFILES)
#	mkdir -p public/data
#	cp -rf $(CP_DATAFILES) public/data/

CP_DATAFILES := $(shell find writtings/data/ -depth)
cp_data_files : $(CP_DATAFILES)
	mkdir -p public/data
	cp -rf writtings/data/* public/data/

# idnexes, directly under root directory *.md files.
ROOT_MD_FILES = $(shell ls writtings/*.md)
root_md_langs: $(ROOT_MD_FILES)
	$(foreach file, $^ , \
	 	pandoc ${file} --to=html5 \
	 	-o $(addprefix public/, $(patsubst %.md, %.html , $(notdir ${file}))) \
	 	$(PANDOC_FLAGS); \
	)

### image of email address
IMAGE_SCRIPT = build/image-gen/text-image-gen.sh
IMAGE_EMAIL = build/image-gen/email-toward-me.png
email_png: # $(IMAGE_SCRIPT) # $(IMAGE_EMAIL)
	bash ${IMAGE_SCRIPT}
	cp ${IMAGE_EMAIL} public/data/


# generate bibliography and links
BIBLIOGRAPHY_TEX = writtings/bibliography.tex
bibliography: $(BIBLIOGRAPHY_TEX)
	pandoc $(BIBLIOGRAPHY_TEX) --to=html5 -o public/bibliography.html \
	--table-of-contents $(PANDOC_FLAGS)

# nilbilus posts
nilbilus_dir = writtings/nilbilus/
posted_files = $(wildcard ${nilbilus_dir}*.md)
nilbilus_posts: ${posted_files}
	$(foreach file, ${posted_files}, \
	pandoc ${file} --to=html \
		-o $(addprefix public/nilbilus/, $(patsubst %.md, %.html , $(notdir ${file}))) \
		$(PANDOC_FLAGS); \
	)

# if something changed in the nilbilus'es data directory
nilbilus_datas_d = $(shell tree -f -i --gitignore --dirsfirst --noreport ${nilbilus_dir}data/)
nilbilus_datas: ${nilbilus_datas_d}
	mkdir -p public/nilbilus/data
	cp -rf ${nilbilus_dir}data/* public/nilbilus/data/


# nilbilus index
nilbilus_index_path = build/tmp/tmp_index.md
nilbilus_index: build/gen_nilbilus_index.py $(sound_recog_20221023_md) $(nenga2022_md)
	python3 build/gen_nilbilus_index.py
	pandoc $(nilbilus_index_path) --to=html5 -o public/nilbilus/index.html \
	$(PANDOC_FLAGS)

# copy_assets
copy_asset: build/style.css build/latex_style.css
	mkdir -p ./public/assets/
	mkdir -p ./public/nilbilus ./public/nilbilus/data
	cp build/latex_style.css ./public/assets/
	cp build/style.css ./public/assets/


# serve
serve:
	python3 -m http.server --directory ./public 8080

# clean
clean:
	rm public/* -rf
