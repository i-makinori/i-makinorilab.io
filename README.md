
# ml.i-makinori.net , Personal Portal site of i-makinori

| Purpose               | URL                                                          |
|-----------------------|--------------------------------------------------------------|
| Current My Domain     | [https://ml.i-makinori.net](https://ml.i-makinori.net)       |
| GitLab pages original | [https://i-makinori.gitlab.io](https://i-makinori.gitlab.io) |

### build

 `public` directory is document root of world wide published in remote server.



```sh
$ make
# to update pages.
$ make serve
# serve to local server.
$ git push 
# to push to remote changes.
$ make clean 
# to clean something.
```

and another,

```sh
$ make email_png 
# to imagemagick and file-replace image of email png.
```


### references

- [GitLab Pages examples / plain-html · GitLab](https://gitlab.com/pages/plain-html)
- Gitlab's URL [Personal Portal](https://i-makinori.gitlab.io/ml.i-makinori.net/)
- Repository : [GitLab Repository](https://gitlab.com/i-makinori/i-makinori.gitlab.io.git)
