---
title: Publishes.  論文・発表などのリスト。
published: true
---



- 池上 蒔典: 「根拠を示しつつ、自ら規則を発見してゆくプログラムの構想。」,  
  [第66回 プログラミング・シンポジウム](https://prosym.org/66/), 2025年01月10日〜01月12日,  
  予稿は情報処理学会 電子図書館にて公開予定,  
  [発表スライド](./data/publishes/2025_prosym1_slide.pdf).
- 池上 蒔典: 「説明可能で発見的な知的プログラムの開発に向けて」,  
  [第二回 若手による数理論理学研究集会](https://sites.google.com/view/yorukai-2nd),2024年8月7日〜8月9日.  
  第2回若手による数理論理学研究集会予稿集(仮),  
  [予稿PDF](./data/publishes/2024_suurironri_wakate_report.pdf),
  [発表スライド](./data/publishes/2024_suurironri_wakate_presentation.pdf).


