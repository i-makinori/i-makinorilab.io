---
title: 音声から文字を書き起こすツールの作成。<br /> 音声から文字を書き起こすツールの作成。
date: 2022-11-01
---

### 何言ってんじゃ だほーい

文字認識 文字 書き起こしツール をプログラム して行こうと思います 

どういたしましては とある本をとある文章館編集に関する本を読んでいる時に音声を文字へと変換するというようなソフトウェアの事例が紹介されていまして 自分でもそのようなツールを利用してみたいと思ったことによります しかしながら そのようなツールはまあ 我々 学生 身分では少々五入しにくいようなところがありまして ならば 自分で作ることはできないのかと思い立ったところが この通りとなります 

食べてみますと音に行くか 日本語においても 文章を文字た失礼しました 音声から 書き起こすようなプログラムは 紹介されていて Python のライブラリを用いて非常に簡単に書かれておりました これをもとに これらを参考に 自分に自分の目的に合致したクリプトを書いて行こうと思い立った次第でワイングッドライトスタンド


システム どういたしましては 8杯 8 システムと致しましては Google のスペシャルアイコン 来ないで 講義内で抗菌できないしね 一緒に猫がいない に大きく依存しております

さわちまー IC レコーダー 部屋だ マイク やらでまずは 音声を拾ってこれをファイルとして保存するとような形式をとってください そして コマンドを走らせます としてコマンドを走らせますとその音声ファイルが まあ 多分 Google の元へと どうされるんですが

まあ 転送されてからしばらく待っていただきますと えーっと AI ちゃんが 平井さんにまあ 書き起こしていただいた文字が流れ出てくるのではないかと思われます そこからですね まあ ローカル側で 文字列処理 形 ディレクトリ 処理 形を ごちゃごちゃして まあ そして結果として まあ一つのファイルが出力されるというような形になっております 

誠しますってはまぁ 今まで自分はキーボードを使ってタイピングをすることで文字を打っていたんですが このように音声から まあ 文字を打ちま 誰かに見てる時もそれを少し修正 するすれば出せると まあこういったアイテムを構築できるようになったというように思いまして まぁ 結果としましてはかなりの文章編集の効率化とかま 思ったこと
浜田せるとかそのようなことはまあ 考えられます まあ 簡単簡単なだけで中身がないともいえるかもしれませんけどまあ

懸念点としまして ははこのシステムは Google に大きく 実績ではなくて 依存している形になっているので
まあ 音声認識 なり 古文の構築 なり まあそういったプログラムも ローカルで出来るようになれば ネットワークへの負荷もかからないですし なにより早く早く まあ 場合によっては確実にできるのではないかなとは思います ちなみに 色々な言語にをサポートしているはずです



### なにってんじゃらほい

##### 動機

 文字認識・文字書き起こしツールを、プログラムしようと思いました。
 動機と致しましては、文書編集に関する本を読んでいる時に、音声を文字へと変換するというようなソフトウェアの事例が紹介されていて、自分でもそのようなツールを利用してみたいと思ったことによります。
 しかしながら、そのようなツールは、少々入手にくいようなと感じ、ならば 自分で作ることはできないのかと、思い立ったところが、この動機となります。
 
 調べてみますと、日本語においても、文章を音声から書き起こすようなプログラムは紹介されていて、Python のライブラリを用いて非常に簡潔に書かれておりました。
 これらを参考に、自分の目的に合致したクリプトを書いて行こうと思い立った次第でワイングッドライトスタンド。

##### システム概要

このシステムはGoogleのspeech_recognitionに大きく依存しております。

ICレコーダー や マイク 等で、まずは、音声を拾ってファイルとして保存します。

コマンドを走らせますと、その音声ファイルが分割されたファイルが Google の元へと 転送されるのですが、
AI ちゃんが 平井さんにまあ 書き起こしていただいた文字が流れ出てくるのではないかと思われます。

ローカル側に文字が落ちてきて、テキスト繋ぎ合わせや、ディレクトリ処理等をごちゃごちゃしますと、結果として一つのテキストファイルが出力されるというような形になっています。

##### 感想

 今まで自分はキーボードにタイピングをすることで文字を打っていたのですが、 このように音声から 文字を起こし、少し修正するすれば、公開できるという、こういったシステムを構築できるようになったというように思いまして、
 
 例えば、文章編集の効率化に繋るのではないかと思われます(簡単なだけで中身が薄くなるとも言われるかも知れませんけど。)。。。。
 
 ちなみに、Googleのspeech_recognitionは色々な自然言語をサポートしているはずです。
 
##### 脆弱性など

懸念点として、このシステムは Google に大きく ~~寄生~~ ではなくて、依存している形になっているので、

サービスが打ち切られた時や、インターネットに繋げられない時などには、使えなくなりますし、  
一度、外部のサービスやインターネットにファイルが流れます。

音声認識や,構文の構築や,古文の構築,等もローカルで出来るようになれば、ネットワークへの負荷もかからないですし なにより早く、場合によっては確実に書き起こせるように成るのではないかなとは思いますが、、、。


##### 感想2

~~、、、とも思いましたが、助詞助動詞がめちゃくちゃで、構文として成立していない場合があり、テキストの全体を能く読んで,直すことになり、日本語の書き起こしでは少し使い難い処があるかも知れません。~~ 誰にでも聞き取れる口調で喋りましょう。AIさんは正直なんです(場合によっては場合によるとは思いますが。)。
 
コンピュータのスクリーンには向かいたく無い一方で、独り言を喋りたい時は、自分にはそれなりにあります。

掛けまくも畏き 音声認識サービス 読み給へ 起こし給えと 恐み恐み白す スクリプトを書き、この記事を起こしつつ、  
考えることと,タイプすることを、全く独立させつつも、情報を受け流すという手段が既に実用されうると感じました。
 
 一度、精度が悪かろうと、音声をテキストに変換できれば、そのテキストの内容を半分書写しながらタイプすることで、以前の発想を再考察しながらも、文書を清書できるというよう、このような編集の流れは、この記事が示す通り、構築されるのではないかとは考えられます。

Googleのspeech_recognition、は単語の流れや傾向を(ある程度の精度で)読み取っており、かなりの高精度で翻訳されているのではないかと感じました。Wonderfullです。  

音声入出力と構文処理を基本の入出力とした、Operating SystemやUser Interfaceの登場も、既にしているのかも知れません。  


##### プログラムの公開


ソースコードはこちらです。 Linux環境でテストされています。  
利用方法等も、こちらを参照下さい。   
[Makinori Ikegami / voice-to-text · GitLab](https://gitlab.com/i-makinori/voice-to-text)

