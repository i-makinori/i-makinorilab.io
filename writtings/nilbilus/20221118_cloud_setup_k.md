---
title: クラウド上へのシステム構築メモ (k殻目)
date: 2022-11-18
---

# クラウド上へのシステム構築メモ

## 背景

中略

- []()

## 現時点 (2022/11/18の14時代+0900頃) での公開中リンク

| 概要                         | 詳細                                                                             | リンク                                                                                                                       |
| --                           | --                                                                               | --                                                                                                                           |
| 新Personal Portalのリンク。  | 旧オレオレGitLab Pages でのコンテンツの移植先でもある。                          | [https://ml.i-makinori.net/](https://ml.i-makinori.net/)                                                                     |
| Source Code 置き場。         | 更に gitweb.cgi へと移動すると、見られるものについては見られる。あはん。         | [https://git.i-makinori.net/](https://git.i-makinori.net/) <br /> [https://lab.i-makinori.net/](https://lab.i-makinori.net/) |
| www.サブドメイン.netである。 | [新Personal Portal](https://ml.i-makinori.net/) へと強制送還される。、、、ハズ。 | [https://www.i-makinori.net/](https://www.i-makinori.net/)                                                                   |



## 設定を開始した動機

1. 禁欲虚しき金欲は、儲かる仕組みを構築した人の話を聞いた。
1. VPSの構築や、独自なwebシステム作成の、経験はあった。
1. クラウド無料クーポンを貰えた。

この3点が重なり、サービスの構築をはじめることになった。


## 動機から考えられた、手段


以下の様に纏められた。

- 色々と配置して、公開できるものは公開すること。
- 書かれたユーティリティの郡について、部分的にやりとりが容易な窓口を用意し、部分的には末端の利用者(エンドユーザ)でも使えるようにする。
- 収益を得ること。



## システム概略



1. 静的なコンテンツは、文書や雑記、ノートや動画やnDモデル、観測・実験データ、スクリプトガカイタデータなどが之に当たる。
    1. (論理的には)独立なサーバが得られていることになる為、(論理的には)重いものもどんどんと入れられる。
1. 動的なコンテンツは、作成されたユーティリティの郡(プログラムの郡)であり、これらのうち、有意と考えられるものについては、インターフェイスを記述し、走行させる。
    1. 対象として、広範な利用者よりは、特定のコミュニティを想定する。
    1. 内部的には、サービス毎に仮想serverとinterfaceを記述する。
1. 静的コンテンツと動的コンテンツを集約し、サーバに運用される。
1. 自分はできる限りサーバ運用に煩わない。


## 設定記録

### サーバの設立申請とOSのインストール

スペックをパチパチと入力すると、そこは、インストール済みCentOSと、SSHの窓口が用意されていた。


- [さくらのクラウド CentOS7を選択してSSH接続までの手順 | mebee](https://mebee.info/2019/11/09/post-3567/)


### rxvt-unicode

SSH クライアント上でも、サーバに rxvt-unicode を入れるだけで、リッチなCLIツールを使えるようになった。  
真相は黒い箱に包まれている。


### 怒りの80番。寡黙なる443番。


httpd(つまりはapache) を入れた。 firewall-cmdを叩いた。

- [How To Open Port 80 on CentOS](https://linuxhint.com/open-port-80-centos/)


80番の他に幾つかの穴をつくった。一度入られたら全部空けられたとしても不思議ではない。


### HTTPS

HTTPSの饅頭は、HTTPのあんこを、HTTPSのあんこで、包むことで料理されている。
便宜上、前者をあんこP,後者をあんこTPと呼ぶ。

VPSを使っていた頃は、3ヶ月位毎に、ぱちぱちとcertbot更新コマンドを叩いていた。  
又、サービスを追加しようとする毎に、何でもできるuserになって、黒い画面にパチパチと打ち込んでいた。  

クラウドのアクセラレータなるものを用いると、幾つかの情報を入力するだけで、
あんこTをあんこTPで包んでくれて,かつ,あんこTPの自動供給までしてくれる、
装置を体の外側に取り付けてくれるらしい。

(Web Serviceプログラマから見て)、  
今までは、手作業で、あんこをあんこで包んでいたとしたら、  
これからは、機械が、あんこをあんこで包んでくれる、、、  
ということになるのだろうか。


- [Let's Encrypt 自動更新証明書の利用 | ウェブアクセラレータ | さくらのクラウド ドキュメント](https://manual.sakura.ad.jp/cloud/webaccel/manual/settings-lets-encrypt-auto-cert.html?_gl=1*updt2u*_ga*MTE4NjA2OTA5MS4xNjY4MDUyMjcw*_ga_CRFPGEHJ0E*MTY2ODUxNTYyOS4xLjAuMTY2ODUxNTYyOS42MC4wLjA.*_ga_M3MLBH78ML*MTY2ODUxNTYyOS42LjAuMTY2ODUxNTYyOS42MC4wLjA.&_ga=2.139083699.1897851377.1668515629-1186069091.1668052270)
- [ドメインのゾーン情報を編集したい | さくらのサポート情報](https://help.sakura.ad.jp/domain/2302/)



### サブドメインの設定。 (仮想サーバ?)

さくらのウェブアクセラレータやドメイン設定より、{ml.<myhost>,lab.<myhost>,www.<myhost>} などと、サイトの追加を申請した。  
encryptの設定と自動更新を外側でやってくれるだけなのに、かなり楽になった気がする。


構造が煩雑になってきたので、apacheから、お馴染のNginxに移行した。 従来から、自作の動的コンテンツをnginx上で運用していた為、今更apacheに移行する必要は無いと判断した。


### ¿¿¿ mail 設定 ???

gitlabでは、mail送信をよくするらしい。 mail送信用の環境設定。


- [Postfixの設定（CentOS 7） | さくらインターネットのVPS設定マニュアル](https://www.sakura-vps.net/centos7-setting-list/postfix-settings-for-sakura-vps-centos7/)
- [Dovecotの設定（CentOS 7） | さくらインターネットのVPS設定マニュアル](https://www.sakura-vps.net/centos7-setting-list/dovecot-settings-for-sakura-vps-centos7/)

### ¿¿¿ GitLab Server ???

- [GitLabをダウンロードしてインストール | GitLab.JP](https://www.gitlab.jp/install/#centos-7)
- [GitLabコマンド コピペで使えるよ！ - Qiita](https://qiita.com/okdokdokdokdokd/items/4d3b44e6f2f30652fb58)

設定でてこずった。

なので、gitserverを用いることにした。


### Git Server and GitWeb

まずは、CGIを有効にし、

- [CentOS 8のNginxでCGIを動かす – fcgiwrap ← RootLinks Co., Ltd.](https://www.rootlinks.net/2020/07/18/centos-8%E3%81%AEnginx%E3%81%A7cgi%E3%82%92%E5%8B%95%E3%81%8B%E3%81%99-fcgiwrap/)

そして、リモートでgitの設定をした。

- [GitサーバとGitWebをCentOS 7で構築する | 晴耕雨読](https://tex2e.github.io/blog/git/git-server-and-gitweb-on-centos7)

なお、apacheの節から調整が入り、Nginxから呼ばれるCGIとして、設定してゆくことになる。

従来のgitlab pagesで公開していたpublicディレクトリを、serverのrootにするように設定した。

`<git_repo_dir>/.git/hook/post-update`を書いた。

なお、自分は、リポジトリのpublicディレクトリ以下を公開していることから、外部のweb観覧アクセスから特定ファイルを隠す操作も不要とした。

`/var/www/gitweb/` に なんちゃら cgi が出来ていたあたり、上記だけでは参考文献が不足している気がする。

- [Public Git Hosting - git.git/tree - gitweb/](https://repo.or.cz/w/git.git/tree/HEAD:/gitweb/)

一様上記が、zip解凍コピペで、(nginx側とprojectrootに) パスを通さば、そのまま表示、gitwebのcgiスクリプトのソースコードである(らしい)。


### Git Server と Nginx で纏められた、静的コンテンツの公開手法

ftpプロトコルを通して、公開用ディレクトリ(`/var/www/html`等)を変更するのではなく、
git コマンドとpushを用いることで(サーバ内部のweb公開用のリポジトリについては更に,`cd n; pull m`,なり`cp this to_there`なりをすることで、)、静的なコンテンツの更新を可能にした。

ローカルとリモートの両方でwebコンテンツの確認ができ、反映も、リモートに(ソースコードとしてであれ、CGIとしてであれ、静的コンテンツであれ)pushさえすれば、できるようにした。(したつもり)。

### 今後について。

何か、CGI(具体的な中身が有る(或るいは無い)サービス)を置く。








