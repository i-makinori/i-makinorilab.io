---
title: Wifi Setup memo on Arch Linux Laptop
date: 2023-02-26
---


self memo for setupping wifi connection on Linux Laptop especially Arch Linux.



### Install Dependencies

```bash
$ pacman -Sy net-tools wpa_supplicant iw iproute wireless_tools
$ pacman -Sy dhclient
```

bellow packages are maybe needed (and also maybely collided).

`netctl(wifi-menu) network-manager-applet nm-cli`



### Device and Scan

```bash
$ ip link
```
to get network devices(interface).

```bash
$ ip link set <device> <up/down>
```
to enable/disable its device,

```bash
$ iwlist scan
```
to scan recognized wireless networks.

```bash
$ islist scan | grep ESSID
```
filters whole lines into SSID only.



### connect via WPA and dhclient.

```bash
$ wpa_passphrase <SSID> <Passphrase> >> /etc/wpa_supplicant.conf
```
to add SSID-Key list into configs file.

you can edit this config file. [^note-i]

```bash
$ wpa_supplicant -B -i <interface> -c /etc/wpa_supplicant.conf
```
to apply configs.
```bash
$ dhclient <interface>
```
to obtain IP address.

[^note-i]: path are changed often
from such a '/etc/wpa_supplicant.conf' into '/etc/wpa_supplicant/wpa_supplicant.conf'



### connection test.

```bash
$ ping <http://somewhere.abc>
```
to check internet connection





### references

- [wpa_supplicant - ArchWiki](https://wiki.archlinux.org/title/wpa_supplicant)
- [Network configuration/Wireless - ArchWiki](https://wiki.archlinux.org/title/Network_configuration/Wireless)
- [■ - 絵のない技術書](https://tokunn.hateblo.jp/entry/2016/06/13/063304)



