---
title: 余生を50年としたら余生は2/3、余生を75年としても余生は3/4、既に死んでいるなら余生は0。
date: 2025-02-17
modified: 2025-02-17
---

もしかしたら、書きながら死んでいたとしても何ら不思議ではない。

### は

世間ではxという数は区切りの良い数だと言われている。なのでおぎゃあの西暦換算でx回目を祈念して、感謝のためあるいは呪詛のため、簡単に何かを記そうと思う。

西暦の日で2月18日が私がおぎゃあした日だと言われている。因みに、母子手帳には午前2:25におぎゃあしたと記録されているらしい。場所は内緒。


小学生未満のときには、エジプトで考古学者になろうだとか、迷路が好きだったり、$ 10^n $ 円 稼ぐことを夢にしていたなどと、口伝や記録が残っていた。  
ちなみに、お母さんをエジプトには連れてはいけないとのこと。その頃にはおばあさんになっているから。との言。

小中高専と0.48を学生らしい学生として過ごす。

<!-- 漁ったら、このようなPDFが出てきた。 -->

7年間引き篭り。


### に

まず、諸縁のあった方々に御礼を申し上げます。

技術を下さり、智恵を下さり、人と人とが触れ合うことによる喜びを下さり、相応の境遇を下さり、課題や道を下さりました。頂いた御恩に報いるべく日々精進し還元して参ります。

次に、目に見えない様々な神霊に御礼や呪詛や適当なうんぬんかんぬんを申し上げます。

数学・計算機・精神世界とをよく勉強してゆき、外的自然や内的内観などを観察して解釈をして、世に還元をしつつも、真理へと接近しようとして参ります。そのうちのひとつの系統は、矛盾が無ければ矛盾という程に矛盾だらけという物でしょうけど。

そして何より、両親に御礼を申し上げます。

両親の現在の私への願いは、経済的な自立でありますし、経済的な面でもの自立は私も望んでいることなので、さしあたっての事としては、この達成を確実なものとすべく、行動として示して参ります。


### ほ


さて、余生の設計を考えようと思う。
生きているという仮定が誤りであるという可能性や、生きているという仮定が真だとしても構想した直後に隕石が脳天に衝突して死ぬ可能性や、
生きているという仮定が真だとしても恒星のガンマ線バーストによる殺菌治療によって $ (キレイ)^2 $ される可能性を考慮すると、
これをする事自体馬鹿馬鹿しいことであるが、何かの間違えで無駄に長く余生を垂らしてまう事も考えられない事ではない。
余生の設計をしよう。このような、いかにして生きるのかという、ないし、
いかにして余生を用いようと意図するのかについては述べはじめるとキリが無く、キリバスでは既に明日で、日本でも明日になってしまいそうなので。ひと事だけ述べて
締めたいと思う。

明日は誰にとっても至って普通の日であるという事を誰にとっても普通は気づかないという程度に誰にとっても至って普通の日。




