---
title: 力学計算・投影Utilityの実装状況報告。
date: 2022-12-07
modified: 2023-02-28
---

### 開発記録。

自分は、[Physical Worlds](https://gitlab.com/i-makinori/physical-worlds)なる、
力学計算・投影Utilityのようなものを開発しようとしています。

その開発状況や開発日記などを、書き置きます。[^1]


| 概要の概要       | リンク                                                                                                                                          |
|------------------|-------------------------------------------------------------------------------------------------------------------------------------------------|
| Repository       | [Physical Worlds](https://gitlab.com/i-makinori/physical-worlds)                                                                                |
| 開発状況の報告   | [20230228_physical-worlds_devreport.pdf](./data/20230228_physical-worlds_devreport.pdf) ([旧版](./data/20221207_physical-worlds_devreport.pdf)) |
| 日記のようなもの | [20230228_physical-worlds_footprint.pdf](./data/20230228_physical-worlds_footprint.pdf) ([旧版](./data/20221207_physical-worlds_footprint.pdf)) |
| 動画             | [physical worlds: spring-model. nawatobi. - YouTube](https://www.youtube.com/watch?v=ZDXIbD3Nsk8)                                               |
| 材料の材料 | [geometry · GitLab](https://gitlab.com/i-makinori/geometry), [wave_practice · GitLab](https://gitlab.com/i-makinori/wave_practice)|


[^1]: この頁は、[Lisp Advent Calendar 2022](https://adventar.org/calendars/7780)の7日目に向けて書きました。
