---
title: Personal Portal
published: true
---

## Makinori Ikegami Personal Portal

- [who are you?](who_are_you.html#English)

#### Publishes

- [publishes](publishes.html)

#### Softwares


- Solution searching program for Shape Filling Puzzle. (2016, 2017, 2024) \
[GitHub](https://github.com/i-makinori/procon_solve)
- Searching and Explaing program for formula transformation of factorization. (2023) \
[GitLab](https://gitlab.com/i-makinori/proof-algebra/)
- An implementation of Fourier Transform. (2023) \
[GitLab](https://gitlab.com/i-makinori/fourier-program)
- Utility for (elementary) mechanics and 3D projection. (2022-) \
[links](/nilbilus/20221207_devreport.html)
- Conway's Game of Life on the FPGA Board. (2022) \
[youtube](https://youtu.be/4ZEnNvheA4o), [lifegame_fpga ・ GitLab](https://gitlab.com/i-makinori/lifegame_fpga)

<!--
- Simpe Wave Simulator(2022) \
[Makinori Ikegami / wave_practice · GitLab](https://gitlab.com/i-makinori/wave_practice)
- Reaction and Diffusion System Simulator(2019) \
[Makinori Ikegami / izumi · GitLab](https://gitlab.com/i-makinori/izumi)
- Real Time/Continuous Artificial Intelligence Simulating Graphical Software(2019 ...) \
[Makinori Ikegami / kotonoha_mikoto · GitLab](https://gitlab.com/i-makinori/kotonoha_mikoto)
- Easy Side Scrolling Game and its Neural Network Agent Program(2017) \
[Makinori Ikegami / coin_gather_game · GitLab](https://gitlab.com/i-makinori/coin_gather_game)
- Cell Automatic Strategy and Influence Simulation(2015) \
[i-makinori/game-theory-simulator](https://github.com/i-makinori/game-theory-simulator)
-->

and [more](composes.html) ...

#### nilbilus

<!--<center>¡¡¡ NIhiLIstIc, NibirIc aNd NiL wLIttINgUS (US meaNS (UN)UPPer Side of US) ¡¡¡</center>-->

- [nilbilus](/nilbilus/)

#### Contact Anchors

- GitLab: [https://gitlab.com/i-makinori](https://gitlab.com/i-makinori)
- GitHub: [https://github.com/i-makinori](https://github.com/i-makinori)
- Email: <img src="data/email-toward-me.png" alt="maau3p at gmail.com  (convert ' at ' into @)" style="display: inline-block; height: 1.5em;"/>


