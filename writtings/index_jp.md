---
title: Personal Portal
<!--author:
  - Makinori Ikegami -->
published: true
---

## Makinori Ikegami Personal Portal

- [わたしはだれ?](who_are_you.html#Nihonngo)

#### 出版・記事

- [publishes](publishes.html)


#### ソフトウェア

- [composes](composes.html)


#### nilbilus

<!--<center>¡¡¡ NIhiLIstIc, NibirIc aNd NiL wLIttINgUS (US meaNS (UN)UPPer Side of US) ¡¡¡</center>-->

- [nilbilus](/nilbilus/)



<!--
#### リンク集
- [俺々Hyper References](./bibliography.html)
-->

#### 連絡先など

- GitLab: [https://gitlab.com/i-makinori](https://gitlab.com/i-makinori)
- GitHub: [https://github.com/i-makinori](https://github.com/i-makinori)
- Email: <img src="data/email-toward-me.png" alt="maau3p at gmail.com  (@ を ' at ' に変えています)" style="display: inline-block; height: 1.5em;"/>
