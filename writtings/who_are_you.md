---
title: 自己紹介
date: 2023-02-27
published: true
---

<!--
;;;;;; ほんの特定の場合に限りおよそ正しい自己紹介
-->

::::{#Nihonngo}
::::

### 自己紹介


私は、池上蒔典(イケガミ マキノリ)という名前が充てられている人間で、

数学や物理や精神世界などに関心があります。

<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> 


計算機としてノイマン型計算機の筐体にLinuxを入れて、Common Lisp, Scheme, Python, C/C++, VerilogHDL, Haskell, Prolog, 日本語, English などの言語を借りて、記号処理プログラム、物理シミュレーション、謎のシミュレーション、組み込みプログラム、謎の文書、などを<!--、偶に、-->書いたり、開発したり、でっち上げたりしています。

今までに作られたものなどは[こちら](./composes.html)に表します。

興味のあるお方は以下のE-Mail Addressまで連絡を下さい。

<img src="data/email-toward-me.png" alt="maau3p at gmail.com  (@ を ' at ' に変えています)" />

--- 池上 蒔典(イケガミ マキノリ) ---

::::{#English}
::::

### Who are you?


My name is Makinori Ikegami.

Interest in Mathematics, Physics, Psychological Space, and etc.....

<br /> <br /> <br /> <br /> <br /> <br /> <br /> <br /> 


When I use Linux which is installed on Von Neumann Architecture,
I use the force of languages such a Common Lisp, Scheme, Python, C/C++, VerilogHDL, Haskell, Prolog, にほんご, Englishish and etc,
programs such as Puzzle Solver, Symbolic Processing, Physics Simulation, Another Simulation and Micro Controller Programs have written.

[here](./composes.html) shows my software in outline.

Anyone Whom interest me, Please Email at below.


<img src="data/email-toward-me.png" alt="maau3p at gmail.com  (convert ' at ' into @)" />

--- Makinori Ikegami ---
