---
title: 製作物など. Composes
published: true
---

私(?)を経由して落とされた、製作物などが並べられます。  
Here lists Softwares wrote via me.


### 製作物


<style>
ul.composes {
  list-style-type: none;
  /*text-align:*/
}
.composes img {
  max-width: 48ex;
  height: auto;
  margin-left: auto;
  margin-right: auto;
}
.composes p {
  text-align: center;
  max-width: 72ex;
}
.composes summary {
  font-weight: bold;
  font-size: 1.2rem;
  margin-top: 1.8rem;
}
</style>


<ul class="composes">
  <li>
      <summary>形状を充填するパズルの解探索プログラム (2016,2017,2024)</summary>
      <p>
        ![shape_filling_puzzle2024](data/puzzle2024.png)
        とあるプログラミングコンテストで出題された問題。  
        一枚の板から切り抜かれた(枠と)ピースの群が与えられます。
        (枠と)ピースとを合成してゆき、もとの平面が復元されたときに解となります。  
        Common Lisp で書きました。  
        [GitHub](https://github.com/i-makinori/procon_solve)
      </p>
  </li>
  <li>
      <summary>因数分解の式変形を探索・説明するプログラム (2023)</summary>
      <p>
        ![factorization2023](data/proof_factorization2023.png)
        記号ベースの探索プログラムとして、証明プログラムを作成しました。  
        開始時の形式から、公理の適用を繰り返して式を変形させてゆき、
        目標の形式となったときに、開始から目標までの変換の流れが証明として返答されます。  
        Common Lisp で書いています。  
        [GitLab](https://gitlab.com/i-makinori/proof-algebra/)
     </p>
  </li>
  <li>
      <summary>フーリエ変換プログラム(2023)</summary>
      <p>
        ![fourier2023](data/spectrogram2023_ufo2.png)
        高速フーリエ変換プログラムや、スペクトログラム描画プログラムを実装しました。  
        Common Lisp を用いています  
        [GitLab](https://gitlab.com/i-makinori/fourier-program)
     </p>
  </li>
  <li>
      <summary>力学計算・投影Utility (2022-)</summary>
      <p>
        ![Physics Simulations](data/physical-worlds2022June_A.png)
        三次元な力学シミュレーターを Common Lisp と Open GL で書いています。  
        線形代数、Quaternion、衝突判定(の一部)、バネの連結モデル、
        万有力学モデルなどを定義・実装しました。  
        [links](/nilbilus/20221207_devreport.html)
      </p>
  </li>
  <li>
      <summary>Conway's Game of Life on the FPGA Board (2022)</summary>
      <p>
        ![Conway’s Game of Life on the FPGA Board](https://img.youtube.com/vi/4ZEnNvheA4o/0.jpg)
        FPGAボード(Terasic DE0)上に  
        ConwayのGame of Lifeを実装しました。  
        VGAモニタにgliderが流れます。  
        [youtube](https://youtu.be/4ZEnNvheA4o), [lifegame_fpga ・ GitLab](https://gitlab.com/i-makinori/lifegame_fpga)
      </p>
  </li>
  <li>
      <summary>簡易的な波動重ね合わせシミュレーション(2022)</summary>
      <p>
        [![](data/wave_practice2022.png)](https://gitlab.com/i-makinori/wave_practice)
        三角関数の重ね合わせシミュレーションです。  
        Common Lisp と Open GL を用いています。  
        [wave_practice · GitLab](https://gitlab.com/i-makinori/wave_practice)
      </p>
  </li>
  <li>
      <summary>反応拡散系シミュレータ(2019)</summary>
      <p>
        [![](data/izumi2019.png)](https://gitlab.com/i-makinori/izumi)  
        近傍の物理量に応じて、濃度が変化する反応をしたり、拡散していったりするモデルの、
        シミュレータです。  
        Common Lisp と McClim を用いています。  
        [izumi · GitLab](https://gitlab.com/i-makinori/izumi)
      </p>
    
  </li>
  <!--
  <li>
      <summary>謎の何かのプログラムの材料とするかもしれない例のアレのプログラム(2019,2020,(20xx?))</summary>
      <p>
        環境と自立的自己書き換えエージェントの実装に向けた Graphical Interface  
        [kotonoha_mikoto · GitLab](https://gitlab.com/i-makinori/kotonoha_mikoto)
      </p>
  </li>
  -->
  <li>  
      <summary>簡単な横スクロールゲーム 及び Neural Network エージェント(2017)</summary>
      <p>
        [![](data/coin_gather_game2017.png)](https://gitlab.com/i-makinori/coin_gather_game)
        PythonとSDLで、コインを集めるゲームを実装しました。
        ゲーム内のエージェントのニューラルネットワークを搭載し、
        教師あり学習をすることができるようになっています。  
        [coin_gather_game · GitLab](https://gitlab.com/i-makinori/coin_gather_game)
      <p>
  </li>
  <li>
      <summary> 戦略と勢力の時間変化シミュレーション(2015) </summary>
      <p>
        [![](data/game-theory-simu2015.png)](https://github.com/i-makinori/game-theory-simulator)
        ゲーム理論を基に作られました。利得表に対する、有利な戦略を観察できます。
        Common Lisp と SDLを用いています。  
        [game-theory-simulator ・ GitHub](https://github.com/i-makinori/game-theory-simulator)
      </p>
  </li>
  <li>
      <summary> 自己ポータルサイト(2017(?)~) </summary>
      <p>
        [自己言及]()
      </p>
  </li>
</ul>
