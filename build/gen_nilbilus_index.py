#/usr/bin/python

import os
import sys
import markdown
import operator

root_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "../")
nilbilus_dir = os.path.join(root_dir, "./writtings/nilbilus/")
public_dir = os.path.join(root_dir, "./public/nilbilus/")
nilbilusrog_index = "index.html"


def safe_key_get(dictio, key):
    try:
        return dictio[key]
    except KeyError:
        return None


index_metadata_text = """
---
title: nilbilus
---

"""

bottom_text = """<!--<center>¡¡¡ NIhiLIstIc, NibirIc aNd NiL wLIttINgUS (US meaNS (UN)UPPer Side of US) ¡¡¡</center>-->"""


def gen_index():

    index_text = ""
    # list of nilbilus files
    # ls nilbilus_dir | grep *.md
    files = list(filter(lambda path: os.path.splitext(path)[1] == ".md",
                        os.listdir(nilbilus_dir)))


    # get metadatas
    file_metadata_list=[]
    md = markdown.Markdown(extensions=['meta'])

    for md_file in files:
        # read file and convert to markdown format
        f = open(os.path.join(nilbilus_dir, md_file),'r') # r:read
        md_file_text = f.read()
        md.convert(md_file_text)
        f.close

        # get global and meta datas
        file_metadata = {
            'filename': md_file,
            'title': safe_key_get(md.Meta, 'title')[0],
            'date': safe_key_get(md.Meta, 'date')[0]
            }
        
        # append to list
        #file_metadata = [filename, date, title, hoge]
        file_metadata_list.append(file_metadata)
    

    # sort by updation Date
    file_metadata_list_sorted = \
        sorted(file_metadata_list,
               key=lambda item: item['date'],
               reverse=False)
    
    # markdown text from each rows
    for meta in file_metadata_list_sorted :
        link = os.path.splitext(meta['filename'])[0] + ".html"
        title, date = meta['title'], meta['date']
        div_right= "style=\"float:right;\""
        index_item = f"- [{title}]({link}) ({link}) <div {div_right}> {date} </div>"
        index_text = index_item + "\n" + index_text

    index_text = index_metadata_text + index_text + bottom_text

    # output to file
    tmp_md_file = os.path.join(os.path.dirname(__file__), "tmp/tmp_index.md")
    
    f = open(tmp_md_file, "w")
    f.write(index_text)
    f.close

if __name__=='__main__':
    
    gen_index()

    sys.exit()
