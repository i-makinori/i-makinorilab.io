#!/usr/bin/python3

# make the template of dialy as text as file.
# file will be written into ./tmp/{year}{month}xx_dialy.md

import os
import copy
import datetime
from dateutil.relativedelta import *


###### constants and variables

(the_year, the_month, the_day) = (2025, 4, 1)
days_reverse_p = True
cur_dir = os.path.dirname(os.path.realpath(__file__))
tmp_dir_dir = cur_dir + "/tmp/"


###### template texts

common_title = "公開日誌"


def dialy_filename(datetime):
    text_template_dialy_filename = '''{y:0>4}{m:0>2}xx_dialy.md'''
    return text_template_dialy_filename.format\
        (y=datetime.year, m=datetime.month)


def template_title(datetime):
    text_template_title = '''{y:0>4}年{m:0>2}月 {common_title}'''
    return text_template_title.format\
        (y=datetime.year, m=datetime.month, d=datetime.day,
         common_title=common_title)


def template_head(datetime):
    text_template_head = '''---
title: {title}
date: {y:0>4}-{m:0>2}-{d}
---

一日毎の予定と反省 など (電子公開版)。

'''

    return text_template_head.format(title=template_title(datetime),
                                     y=datetime.year, m=datetime.month, d=datetime.day)


def template_per_aday(datetime):
    text_template_per_aday = '''###### {y:0>4}年{m:0>2}月{d:0>2}日({x})
- 予定
- 反省
- その他

'''
    return text_template_per_aday.format\
        (y=datetime.year,
         m=datetime.month,
         d=datetime.day,
         x=datetime_weekday_text(datetime))


def template_cont(datetime_center):
    text_template_cont = '''
- 次の月: お楽しみに! <!-- [{title_n}]({filename_n}) -->
- 前の月: お楽しみに! <!-- [{title_p}]({filename_p}) -->
'''
    next_month_of_days = datetime_center + relativedelta(months=+1)
    prev_month_of_days = datetime_center - relativedelta(months=+1)

    return text_template_cont.format\
        (title_n = template_title(next_month_of_days),
         filename_n = dialy_filename(next_month_of_days),
         title_p = template_title(prev_month_of_days),
         filename_p = dialy_filename(prev_month_of_days))

def template_head_cont(datetime_center):
    return template_cont(datetime_center) + "\n\n"

def template_tail(datetime_center):
    tail_text = '''
### 接続

'''
    return tail_text + template_cont(datetime_center)



def datetime_weekday_text(datetime):
    weekday_texts = ["虚", "月","火", "水", "木", "金", "土", "日", "無"]
    return weekday_texts[datetime.isoweekday()]


###### apply templates

def make_dialy_template_text(datetime_start):
    var_datetime = copy.copy(datetime_start)
    var_template_text = ''''''

    # apply template of head
    var_template_text = template_head(var_datetime)
    var_template_text += template_head_cont(datetime_start)

    # make template of each days as day_array.
    content_of_each_days = []
    content_of_a_day = ""

    while(var_datetime.month == the_month):
        content_of_a_day = template_per_aday(var_datetime)
        content_of_each_days += [content_of_a_day]
        var_datetime = var_datetime + datetime.timedelta(days=1)

    if(days_reverse_p): content_of_each_days.reverse()

    # apply template of each days
    for aday in content_of_each_days:
        var_template_text += aday

    # apply template of tail
    var_template_text += template_tail(datetime_start)

    # return
    return var_template_text


###### main

if __name__ =="__main__":
    var_datetime = datetime.datetime(the_year, the_month, the_day)

    #### make template
    var_template_text = make_dialy_template_text(var_datetime)
    print(var_template_text)

    #### write to file
    tmp_dialy_file_name = dialy_filename(var_datetime)
    path = tmp_dir_dir + tmp_dialy_file_name

    f = open(path, "w")
    f.write(var_template_text)
    f.close()

    ### finish
    print("-------\ntemplate has written into {}.".format(path))
